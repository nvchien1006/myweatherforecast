package com.example.weatherforecast.view.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.weatherforecast.R;
import com.example.weatherforecast.ultis.Common;
import com.example.weatherforecast.view.dialog.CheckGPSDialog;
import com.example.weatherforecast.view.fragment.MainWetherWether;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class MainActivity extends BaseActivity
        implements EasyPermissions.PermissionCallbacks {

    public static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 100;
    private static final int RC_LOCAL_PERM = 1;

    FusedLocationProviderClient mFusedLocationClient;
    private CheckGPSDialog checkGPSDialog;
    public boolean isGetedLocal = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        isGetedLocal = false;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        registerBroadcastGpsOn();
        registerBroadcastConnectOn();

    }

    private BroadcastReceiver conectionBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getCurentLocal();
        }
    };

    private void registerBroadcastConnectOn() {
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(conectionBroadcastReceiver, intentFilter);
    }

    private void unRegisterBroadcastConnectOn() {
        unregisterReceiver(conectionBroadcastReceiver);
    }

    private BroadcastReceiver gpsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            getCurentLocal();
        }
    };

    private void registerBroadcastGpsOn() {
        IntentFilter intentFilter = new IntentFilter("android.location.PROVIDERS_CHANGED");
        registerReceiver(gpsBroadcastReceiver, intentFilter);
    }

    private void unRegisterBroadcastGpsOn() {
        unregisterReceiver(gpsBroadcastReceiver);
    }


    @AfterPermissionGranted(RC_LOCAL_PERM)
    public void getCurentLocal() {
        if (hasLocalPermisstion()) {
            if (Common.isGpsAvailable(this)) {
                if (Common.isNetworkAvailable(this)) {
                    if (!isGetedLocal) {
                        mFusedLocationClient.getLastLocation().addOnCompleteListener(
                            new OnCompleteListener<Location>() {
                                @Override
                                public void onComplete(@NonNull Task<Location> task) {

                                    Location location = task.getResult();
                                    if (location == null) {
                                        requestNewLocationData();
                                    } else {
                                        isGetedLocal = true;
                                        showWeatherIn(location);
                                    }
                                }
                            }
                        );
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Network has been turn off", Toast.LENGTH_SHORT).show();
                }
            } else {
                showCheckGPSDialog();
            }
        } else {
            EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.rationale_local),
                    RC_LOCAL_PERM,
                    Manifest.permission.ACCESS_FINE_LOCATION);
        }
    }

    private void requestNewLocationData() {

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            showWeatherIn(mLastLocation);

        }
    };

    private void showWeatherIn(Location mLastLocation) {

        MainWetherWether.openCurWetherNotTransaction(MainActivity.this,
                (float) mLastLocation.getLongitude(), (float) mLastLocation.getLatitude());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);

    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        Toast.makeText(this, "Failed to get permisson ", Toast.LENGTH_SHORT).show();

    }

    private boolean hasLocalPermisstion() {
        return EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                getCurentLocal();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                LatLng latLong = place.getLatLng();
                float lat = (float) latLong.latitude;
                float lon = (float) latLong.longitude;
                MainWetherWether.openCurWetherNotTransaction(this, lon, lat);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    private void getLocal() {

        List<Place.Field> fields = Arrays.asList(Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    private void showCheckGPSDialog() {
        if (checkGPSDialog == null) {
            checkGPSDialog = new CheckGPSDialog(this);
        }
        checkGPSDialog.showCheckGPSDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastConnectOn();
        unRegisterBroadcastGpsOn();

    }
}
