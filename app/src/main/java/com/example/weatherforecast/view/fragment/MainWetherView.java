package com.example.weatherforecast.view.fragment;

import com.example.weatherforecast.model.entity.Weather;

import java.util.ArrayList;

public interface MainWetherView {
    public void updateWetherView(Weather wetherModel);
    public void updateForecast(ArrayList<Weather> weathers);
}
