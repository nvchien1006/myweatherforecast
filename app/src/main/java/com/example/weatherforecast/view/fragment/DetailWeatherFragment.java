package com.example.weatherforecast.view.fragment;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.weatherforecast.other.UrlWeather;
import com.example.weatherforecast.R;
import com.example.weatherforecast.model.MainWetherManager;
import com.example.weatherforecast.model.entity.Weather;
import com.example.weatherforecast.view.activity.MainActivity;
import com.google.gson.Gson;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

//in = JSON string detail
//out = show infor

public class DetailWeatherFragment extends BasicFragment {
    private static final String BUNDLE_WETHERDETAIL = "BUNDLE_WETHERDETAIL";

    private String weatherDetailJSONString;
    @BindView(R.id.tv_detail_day)
    TextView dayTv;
    @BindView(R.id.tv_detail_date)
    TextView dateTv;
    @BindView(R.id.tv_detail_temperature_from)
    TextView temperatureFrom;
    @BindView(R.id.tv_detail_temperature_to)
    TextView temperatureTo;
    @BindView(R.id.imv_detail_weather)
    ImageView thumbImg;
    @BindView(R.id.tv_detail_des)
    TextView desTv;
    @BindView(R.id.tv_detail_humidity)
    TextView humidityTv;
    @BindView(R.id.tv_detail_pressure)
    TextView pressureTv;
    @BindView(R.id.tv_detail_wind)
    TextView windTv;
    @BindView(R.id.img_detail_background)
    ImageView bgImg;


    public DetailWeatherFragment() {
        // Required empty public constructor
    }

    private static DetailWeatherFragment newInstance(String weatherDetail) {
        DetailWeatherFragment fragment = new DetailWeatherFragment();
        Bundle args = new Bundle();
        args.putString(BUNDLE_WETHERDETAIL, weatherDetail);
        fragment.setArguments(args);
        return fragment;
    }

    static void openDetailFragment(MainActivity mainActivity, String weatherDetail) {
        DetailWeatherFragment detailWeatherFragment = newInstance(weatherDetail);
        goNextScreen(mainActivity, detailWeatherFragment);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            weatherDetailJSONString = getArguments().getString(BUNDLE_WETHERDETAIL);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            backHome();
        }
        return super.onOptionsItemSelected(item);
    }

    private void backHome() {
        if(getActivity()!=null)
            backPress((MainActivity) getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_detail_wether, container, false);

        ButterKnife.bind(this, v);
        showTopNavigation();
        displayWetherDetail(weatherDetailJSONString);

        return v;

    }

    private void displayWetherDetail(String weatherDetailJSONString) {
        Gson gson = new Gson();
        Weather weather = gson.fromJson(weatherDetailJSONString, Weather.class);
        dayTv.setText(weather.getHourOfDay()+" "+weather.getDay());
        dateTv.setText(weather.getDate());
        Glide.with(Objects.requireNonNull(getActivity()))
                .load(UrlWeather.ICON_WEATHER_URL + weather.getIcon() + ".png")
                .transition(DrawableTransitionOptions.withCrossFade(400))
                .into(thumbImg);
        temperatureFrom.setText(weather.getTemperutareFrom());
        temperatureTo.setText(weather.getTemperutareTo());

        String status = weather.getDescription();
        desTv.setText(status);
        Glide.with(this).load(MainWetherManager.getBackgound(status))
                .transition(DrawableTransitionOptions.withCrossFade(400))
                .into(bgImg);

        humidityTv.setText(weather.getHumidity());
        pressureTv.setText(weather.getPressure());
        windTv.setText(weather.getWind());
        /// chance b

    }

    @Override
    public void onResume() {

        super.onResume();

        backPress();
    }

    private void backPress() {
        Objects.requireNonNull(getView()).setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                backHome();

                return true;

            }
            return false;
        });
    }


}
