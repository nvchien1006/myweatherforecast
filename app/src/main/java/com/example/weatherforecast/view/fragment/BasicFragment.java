package com.example.weatherforecast.view.fragment;

import android.app.Activity;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.weatherforecast.R;
import com.example.weatherforecast.view.activity.MainActivity;

public class BasicFragment extends Fragment {

    static public void goNextScreen(MainActivity mainActivity, Fragment fragment) {
        mainActivity.getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.enter_from_right, R.anim.exit_to_left,
                        R.anim.enter_from_left, R.anim.exit_to_right)
                .replace(R.id.framelayout_main_containfragment, fragment)
                .addToBackStack(null)
                .commit();
    }
    static public void goNextScreenNotTransaction(MainActivity mainActivity, Fragment fragment) {
        mainActivity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.framelayout_main_containfragment, fragment)
                .addToBackStack(null)
                .commit();
    }

    public void hideTopNavigation() {
        showUpNavigation(false);
    }
    public void showTopNavigation() {
        showUpNavigation(true);
    }

    private void showUpNavigation(boolean isShowNavigation) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null && mainActivity.getSupportActionBar() != null) {
            mainActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(isShowNavigation);
            mainActivity.getSupportActionBar().setDisplayShowHomeEnabled(isShowNavigation);
        }
    }

    public static void backPress(MainActivity mainActivity) {
        mainActivity.getSupportFragmentManager().popBackStack();
    }

}
