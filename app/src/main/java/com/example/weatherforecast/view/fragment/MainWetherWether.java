package com.example.weatherforecast.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.example.weatherforecast.adapter.ForcastAdapter;
import com.example.weatherforecast.other.UrlWeather;
import com.example.weatherforecast.R;
import com.example.weatherforecast.model.MainWetherManager;
import com.example.weatherforecast.model.entity.Weather;
import com.example.weatherforecast.ultis.Common;
import com.example.weatherforecast.view.dialog.LoadingDialog;
import com.example.weatherforecast.view.activity.MainActivity;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainWetherWether extends BasicFragment
        implements MainWetherView {

    @BindView(R.id.tv_main_temperature)
    TextView temperatureTv;
    @BindView(R.id.tv_main_city)
    TextView cityTv;
    @BindView(R.id.tv_main_des)
    TextView desTv;
    @BindView(R.id.tv_main_temperature_max)
    TextView tempMaxTv;
    @BindView(R.id.tv_main_temperature_min)
    TextView tempMinTv;
    @BindView(R.id.tv_main_date)
    TextView dateTv;
    @BindView(R.id.imv_main_weather)
    ImageView weatherIconImg;
    @BindView(R.id.img_main_bg)
    ImageView bgImgView;
    @BindView(R.id.rcv_main_information)
    RecyclerView forecastRecyclerView;

    private static final String ARG_LAT = "ARG_LAT";
    private static final String ARG_LON = "ARG_LON";

    private View rootView;
    private ForcastAdapter forcastAdapter;
    private ArrayList<Weather> weathers;
    private LoadingDialog loadingDialog;
    private Weather curWeather;

    private MainWetherManager mainWetherManager;
    private float mLon;
    private float mLat;

    public MainWetherWether() {
        // Required empty public constructor
    }

    public static MainWetherWether newInstance() {
        MainWetherWether fragment = new MainWetherWether();
        return fragment;
    }

    public static MainWetherWether newInstance(float mLon, float mLat) {
        MainWetherWether fragment = new MainWetherWether();
        Bundle args = new Bundle();
        args.putFloat(ARG_LAT, mLat);
        args.putFloat(ARG_LON, mLon);
        fragment.setArguments(args);
        return fragment;
    }

    public static MainWetherWether openCurWether(MainActivity mainActivity, float mLon, float mLat) {
        MainWetherWether fragment = newInstance(mLon, mLat);
        goNextScreen(mainActivity, fragment);

        return fragment;
    }

    public static MainWetherWether openCurWetherNotTransaction(MainActivity mainActivity, float mLon, float mLat) {
        MainWetherWether fragment = newInstance(mLon, mLat);
        goNextScreenNotTransaction(mainActivity, fragment);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG", "onCreate: ");

        setHasOptionsMenu(true);
        mLon = getArguments().getFloat(ARG_LON);
        mLat = getArguments().getFloat(ARG_LAT);

        weathers = new ArrayList<>();
        mainWetherManager = new MainWetherManager(getActivity(), this);
        loadWetherDataFromServer();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, rootView);
        Log.d("TAG", "onCreateView: ");


        initRecycleView();
        hideTopNavigation();
        if(curWeather!=null) {
            updateCurWeatherUI(curWeather);
        }

        return rootView;
    }

    private void loadWetherDataFromServer() {
        if (loadingDialog == null) {
            loadingDialog = new LoadingDialog(getContext());
        }
        loadingDialog.showWindow();
        mainWetherManager.getDataWether(mLon, mLat);
    }

    private void initRecycleView() {

        forcastAdapter = new ForcastAdapter(getActivity(), weathers, new ForcastAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                Gson gson = new Gson();
                String wetherDetail = gson.toJson(weathers.get(posItem));
                DetailWeatherFragment.openDetailFragment((MainActivity) getActivity(), wetherDetail);
            }
        });
        forecastRecyclerView.setAdapter(forcastAdapter);
//2
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        forecastRecyclerView.setLayoutManager(linearLayoutManager);

//3
//        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
//        rcv.addItemDecoration(dividerItemDecoration);

//4
        forecastRecyclerView.setItemAnimator(new DefaultItemAnimator());

    }

    @Override
    public void updateWetherView(Weather weather) {
        curWeather = weather;
        updateCurWeatherUI(weather);

    }

    private void updateCurWeatherUI(Weather weather) {
        temperatureTv.setText(weather.getTemp());
        tempMinTv.setText(weather.getTemperutareFrom());
        tempMaxTv.setText(weather.getTemperutareTo());

        Glide.with(this).load(MainWetherManager.getBackgound(weather.getDescription()))
                .transition(DrawableTransitionOptions.withCrossFade(400))
                .into(bgImgView);
        dateTv.setText(weather.getDate());
        Glide.with(Objects.requireNonNull(getActivity())).load(UrlWeather.ICON_WEATHER_URL + weather.getIcon() + ".png")
                .transition(DrawableTransitionOptions.withCrossFade(400))
                .into(weatherIconImg);


        desTv.setText(weather.getDescription());
        try {
            cityTv.setText(MainWetherManager.getNameCityFromLocal(getActivity(),mLon,mLat));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateForecast(ArrayList<Weather> weathers) {
        loadingDialog.closeWindow();
        this.weathers.clear();
        this.weathers.addAll(weathers);
        forcastAdapter.notifyDataSetChanged();
    }


    //---------<menu
    @Override
    public void onCreateOptionsMenu(Menu menu,@NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_refresh, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                if (Common.isNetworkAvailable(getActivity())) {
                    refresh();
                }else {
                    Toast.makeText(getActivity(), "Network has been turn off", Toast.LENGTH_SHORT).show();
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    //------ </menu
    private void refresh() {
        loadWetherDataFromServer();
    }



}
