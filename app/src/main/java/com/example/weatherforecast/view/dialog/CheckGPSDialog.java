package com.example.weatherforecast.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;

public class CheckGPSDialog {
     private Dialog alertDialog;

    public CheckGPSDialog(Context context) {
        this.alertDialog = newInstance(context);
    }

    public Dialog newInstance(Context mContext) {
        AlertDialog.Builder builder;
        if(alertDialog==null) {
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle("GPS Not Enabled");
            builder.setMessage("Do you wants to turn On GPS");

            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    mContext.startActivity(intent);
                    dialog.dismiss();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog = builder.create();

        }
        return alertDialog;
    }

    public void showCheckGPSDialog()  {
        if (alertDialog != null && !alertDialog.isShowing()) {
            alertDialog.show();
        }
    }
}
