package com.example.weatherforecast.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.weatherforecast.other.UrlWeather;
import com.example.weatherforecast.R;
import com.example.weatherforecast.model.entity.Weather;

import java.util.List;

public class ForcastAdapter extends RecyclerView.Adapter<ForcastAdapter.ViewHolder> {

    private Context context;
    private List<Weather> weathers;
    private ClickItemRcv clickItemRcv;

    public interface ClickItemRcv {
        void onClickItemRcv(int posItem);
    }

    public ForcastAdapter(Context context, List<Weather> weathers, ClickItemRcv clickItemRcv) {
        this.context = context;
        this.weathers = weathers;
        this.clickItemRcv = clickItemRcv;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main_forecast, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        //
        Weather weather = weathers.get(i);
        Glide.with(context).load(UrlWeather.ICON_WEATHER_URL + weather.getIcon() + ".png").into(viewHolder.statusForcastImg);
        viewHolder.dayForcastTv.setText(weather.getHourOfDay()+" "+weather.getDay().substring(0,3) + "");
        viewHolder.desForcastTv.setText(weather.getDescription() + "");
        viewHolder.temperatureFromForcastTv.setText(weather.getTemperutareFrom() + "");
        viewHolder.temperatureToForcastTv.setText(weather.getTemperutareTo() + "");


    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //1
        private ImageView statusForcastImg;
        private TextView dayForcastTv;;
        private TextView desForcastTv;;
        private TextView temperatureFromForcastTv;;
        private TextView temperatureToForcastTv;;

        //2
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            //3
            itemView.setOnClickListener(this);

            //4
            statusForcastImg = itemView.findViewById(R.id.imv_main_status);
            dayForcastTv = itemView.findViewById(R.id.tv_main_day);
            desForcastTv = itemView.findViewById(R.id.tv_main_des);
            temperatureFromForcastTv = itemView.findViewById(R.id.tv_main_temperature_from);
            temperatureToForcastTv = itemView.findViewById(R.id.tv_main_temperature_to);

        }

        //5
        @Override
        public void onClick(View v) {
            try {

                int po = getAdapterPosition();
                if (clickItemRcv != null) {
                    clickItemRcv.onClickItemRcv(po);
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

}
