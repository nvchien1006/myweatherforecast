package com.example.weatherforecast.other;

/**
 * Created by VjrutNAT on 12/29/2016.
 */

public class UrlWeather {

    public static final String ICON_WEATHER_URL = "http://openweathermap.org/img/w/";

    public static final String locationWeatherUrl(float lon, float lat){
        return "https://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=8d1ff322ac77a4be2c80b01d8969dddb";
    }

    public static final String locationWeekWeatherUrl(float lon, float lat){
        return "http://api.openweathermap.org/data/2.5/forecast?lat="+lat+"&lon="+lon+"&appid=8d1ff322ac77a4be2c80b01d8969dddb";
    }

}
