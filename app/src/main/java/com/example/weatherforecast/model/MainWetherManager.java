package com.example.weatherforecast.model;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.weatherforecast.other.UrlWeather;
import com.example.weatherforecast.other.VolleySingleton;
import com.example.weatherforecast.R;
import com.example.weatherforecast.ultis.ConvertUltis;
import com.example.weatherforecast.model.entity.Weather;
import com.example.weatherforecast.view.fragment.MainWetherView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static com.android.volley.VolleyLog.TAG;
import static com.example.weatherforecast.ultis.ConvertUltis.getMonthName;

// xu ly logic
public class MainWetherManager {


    private final static int CONST_KENVIN_TO_C = -273;
    private Activity activity;
    private MainWetherView mainWetherView;

    public MainWetherManager(Activity activity, MainWetherView mainWetherView) {
        this.activity = activity;
        this.mainWetherView = mainWetherView;
    }

    public void getDataWether(float mLon, float mLat) {
        getCurrentWether(mLon, mLat);
        getForecastWeather(mLon, mLat);
    }

    private void getCurrentWether(final float mLon, final float mLat) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(UrlWeather.locationWeatherUrl(mLon, mLat), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Weather weatherDetails = getWeatherFromJSON(response);
                mainWetherView.updateWetherView(weatherDetails);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // do nothing
            }
        });
        VolleySingleton.getInstance(activity).getRequestQueue().add(jsonObjectRequest);
    }

    private void getForecastWeather(float mLon, float mLat) {

        JsonObjectRequest objectRequest = new JsonObjectRequest(UrlWeather.locationWeekWeatherUrl(mLon, mLat), null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: forcast " + response.toString());
                JSONArray forecastList = response.optJSONArray("list");
                ArrayList<Weather> weathers = new ArrayList<>();

                for (int i = 0; i < forecastList.length(); i++) {
                    JSONObject forecast = forecastList.optJSONObject(i);

                    Weather weatherDetails = getWeatherFromJSON(forecast);
                    weathers.add(weatherDetails);
                }
                mainWetherView.updateForecast(weathers);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        VolleySingleton.getInstance(activity).getRequestQueue().add(objectRequest);

    }

    private Weather getWeatherFromJSON(JSONObject forecast) {
        JSONObject main = forecast.optJSONObject("main");
        int temp = main.optInt("temp");
        int tempMax = main.optInt("temp_max");
        int tempMin = main.optInt("temp_min");
        int pressure = main.optInt("pressure");
        int humidity = main.optInt("humidity");

        JSONObject wind = forecast.optJSONObject("wind");
        double speed = wind.optDouble("speed");

        String date = forecast.optString("dt");

        JSONArray weather = forecast.optJSONArray("weather");
        JSONObject objWeather = weather.optJSONObject(0);
        String description = objWeather.optString("description");
        String icon = objWeather.optString("icon");

        Calendar calendar = Calendar.getInstance();
        TimeZone tz = TimeZone.getDefault();
        calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
        Date currentTimeZone = new Date((long) Integer.parseInt(date) * 1000);
        calendar.setTime(currentTimeZone);
        int hourOfDay =  calendar.get(Calendar.HOUR_OF_DAY);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        int mMonth = calendar.get(Calendar.MONTH);
        String dateFormat =   mDay+ " "+getMonthName(mMonth) ;
        return new Weather(
                hourOfDay+"h",
                ConvertUltis.getDayName(dayOfWeek),
                description,
                (temp + CONST_KENVIN_TO_C) + " °C",
                (tempMin + CONST_KENVIN_TO_C) + " °C",
                (tempMax + CONST_KENVIN_TO_C) + " °C",
                humidity + " %",
                pressure + " hPa",
                speed + " m/s",
                icon,
                dateFormat);
    }

    public static String getNameCityFromLocal(Activity activity, float mLon, float mLat) throws IOException {
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        addresses = geocoder.getFromLocation(mLat, mLon, 1);
        if(addresses.size()==0) return "";
        String address = addresses.get(0).getAddressLine(0);
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();
        if (state != null) {
            return state + ", " + country;
        } else {
            return city + ", " + country;
        }
    }

    public static int getBackgound(String description) {
        switch (description) {
            case "broken clouds":
                return R.drawable.brokenclouds;
            case "clear sky":
                return R.drawable.clearsky;
            case "few clouds":
                return R.drawable.fewclouds;
            case "mist":
                return R.drawable.mist;
            case "rain":
                return R.drawable.rain;
            case "scattered clounds":
                return R.drawable.scatteredclouds;
            case "shower rain":
                return R.drawable.showerrain;
            case "snow":
                return R.drawable.snow;
            case "thunder storm":
                return R.drawable.thunderstorm;
            case "drizzle":
                return R.drawable.drizzle;
            case "dust":
                return R.drawable.dust;
            case "fog":
                return R.drawable.fog;
            case "tormado":
                return R.drawable.tornado;
            case "drizzle rain":
                return R.drawable.drizzlerain;
            case "extreme rain":
                return R.drawable.extremerain;
            case "freezing rain":
                return R.drawable.freezingrain;
            case "heavy intensity drizzle rain":
                return R.drawable.heavyintensitydrizzlerain;
            case "heavy intensity drizzle":
                return R.drawable.heavyintensitydrizzle;
            case "heavy intensity rain":
                return R.drawable.heavyintensityrain;
            case "heavy intensity shower rain":
                return R.drawable.heavyintensityshowerrain;
            case "heavy shower rain and drizzle":
                return R.drawable.heavyshowerrainanddrizzle;
            case "heavy shower snow":
                return R.drawable.heavyshowersnow;
            case "heavy snow":
                return R.drawable.heavysnow;
            case "light intensity drizzle":
                return R.drawable.lightintensitydrizzle;
            case "light intensity shower rain":
                return R.drawable.lightintensitydrizzlerain;
            case "light rain and snow":
                return R.drawable.lightrainandsnow;
            case "light shower snow":
                return R.drawable.lightshowersnow;
            case "light rain":
                return R.drawable.lightrain;
            case "light snow":
                return R.drawable.lightsnow;
            case "light thunderstorm":
                return R.drawable.lightthunderstorm;
            case "moderate rain":
                return R.drawable.moderaterain;
            case "overcast clouds":
                return R.drawable.overcastclouds;
            case "ragged shower rain":
                return R.drawable.raggedshowerrain;
            case "rain and snow":
                return R.drawable.rainandsnow;
            case "sand, dust whirls":
                return R.drawable.sanddustwhirls;
            case "shower drizzle":
                return R.drawable.showerdrizzle;
            case "shower rain and drizzle":
                return R.drawable.showerrainanddrizzle;
            case "shower sleet":
                return R.drawable.showersleet;
            case "shower snow":
                return R.drawable.showersnow;
            case "thunderstorm with drizzle":
                return R.drawable.thunderstormwithdrizzle;
            case "ragged thunderstorm":
                return R.drawable.raggedthunderstorm;
            case "thunderstorm with heavy drizzle":
                return R.drawable.thunderstormwithheavydrizzle;
            case "thunderstorm with heavy rain":
                return R.drawable.thunderstormwithheavyrain;
            case "thunderstorm with light drizzle":
                return R.drawable.thunderstormwithlightdrizzle;
            case "thunderstorm with light rain":
                return R.drawable.thunderstormwithlightrain;
            case "thunderstorm with rain":
                return R.drawable.thunderstormwithrain;
            case "very heavy rain":
                return R.drawable.veryheavyrain;
            case "volcani cash ":
                return R.drawable.volcanicash;
            case "heavy thunderstorm":
                return R.drawable.heavythunderstorm;
            case "light intensity drizzle rain":
                return R.drawable.lightintensitydrizzlerain;
            default://sky is clear
                return R.drawable.scatteredclouds;
        }
    }


}
