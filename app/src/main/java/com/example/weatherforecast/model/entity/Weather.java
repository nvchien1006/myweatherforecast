package com.example.weatherforecast.model.entity;

/**
 * Created by VjrutNAT on 12/28/2016.
 */

public class Weather {

    private String hourOfDay;
    private String day;
    private String description;
    private String temp;
    private String temperutareFrom;
    private String temperutareTo;
    private String humidity;
    private String pressure;
    private String wind;
    private String icon;
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Weather(String hourOfDay, String day, String description, String temp, String temperutareFrom,
                   String temperutareTo, String humidity, String pressure, String wind,
                   String icon, String date) {
        this.hourOfDay = hourOfDay;
        this.day = day;
        this.description = description;
        this.temp = temp;
        this.temperutareFrom = temperutareFrom;
        this.temperutareTo = temperutareTo;
        this.humidity = humidity;
        this.pressure = pressure;
        this.wind = wind;
        this.icon = icon;
        this.date = date;
    }
    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getWind() {
        return wind;
    }

    public void setWind(String wind) {
        this.wind = wind;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTemperutareFrom() {
        return temperutareFrom;
    }

    public void setTemperutareFrom(String temperutareFrom) {
        this.temperutareFrom = temperutareFrom;
    }

    public String getTemperutareTo() {
        return temperutareTo;
    }

    public void setTemperutareTo(String temperutareTo) {
        this.temperutareTo = temperutareTo;
    }

    public Weather(String day, String description, String temperutareFrom, String temperutareTo, String icon) {
        this.day = day;
        this.description = description;
        this.temperutareFrom = temperutareFrom;
        this.temperutareTo = temperutareTo;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getHourOfDay() {
        return hourOfDay;
    }

    public void setHourOfDay(String hourOfDay) {
        this.hourOfDay = hourOfDay;
    }
}
